package com.example.android64;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MovePhoto extends AppCompatActivity {

    public static final String CURRENT_ALBUM_INDEX = "current_album_index";
    public static final String ALBUMS_LIST = "albums_list";

    private ListView albumsListView;
    private Button moveButton;
    private Button copyButton;

    private ArrayList<Album> albumsMovable;
    private Album selectedAlbum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_photo_container);

        albumsMovable = new ArrayList<Album>();

        albumsListView = findViewById(R.id.albumsToMoveListView);
        moveButton = findViewById(R.id.movePhotoConfirmButton);
        copyButton = findViewById(R.id.copyPhotoConfirmButton);
        enableMoveCopyButtons(selectedAlbum != null);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ArrayList<Album> allAlbums = bundle.getParcelableArrayList(ALBUMS_LIST);
            int selectedAlbumIndex = bundle.getInt(CURRENT_ALBUM_INDEX);
            for (int i = 0; i < allAlbums.size(); ++i) {
                if (i != selectedAlbumIndex) {
                    albumsMovable.add(allAlbums.get(i));
                }
            }
        } else {
            Toast toast = Toast.makeText(this, "Other albums could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        albumsListView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albumsMovable));
        albumsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectAlbum(position);
            }
        });

    }

    private void enableMoveCopyButtons(boolean enable) {
        if (!enable) {
            selectedAlbum = null;
        }
        moveButton.setEnabled(enable);
        moveButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        copyButton.setEnabled(enable);
        copyButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    private void selectAlbum(int index) {
        selectedAlbum = albumsMovable.get(index);
        enableMoveCopyButtons(true);
    }

    public void confirmMove(View view) {
        Bundle bundle = new Bundle();
        bundle.putString(Photos.COPY_OR_MOVE, "move");
        bundle.putString(Photos.MOVE_ALBUM, selectedAlbum.getName());
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void confirmCopy(View view) {
        Bundle bundle = new Bundle();
        bundle.putString(Photos.COPY_OR_MOVE, "copy");
        bundle.putString(Photos.MOVE_ALBUM, selectedAlbum.getName());
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

}

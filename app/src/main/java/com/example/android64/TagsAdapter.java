package com.example.android64;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class TagsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Tag> tags;

    public TagsAdapter(Context context, ArrayList<Tag> tags) {
        this.context = context;
        this.tags = tags;
    }

    @Override
    public int getCount() {
        return this.tags.size();
    }

    @Override
    public Object getItem(int i) {
        return this.tags.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewTemp temp;
        if (view == null) {
            temp = new ViewTemp();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.tag_list_item, null, true);
            temp.tagType = (TextView)  view.findViewById(R.id.itemTagType);
            temp.tagValue = (TextView)  view.findViewById(R.id.itemTagValue);
            view.setTag(temp);
        } else {
            temp = (ViewTemp) view.getTag();
        }
        temp.tagType.setText(this.tags.get(i).getTagType());
        temp.tagValue.setText(this.tags.get(i).getTagValue());
        return view;
    }

    private class ViewTemp {
        TextView tagType, tagValue;
    }
}

package com.example.android64;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AddTag extends AppCompatActivity {

    public static final String TAGS_LIST = "tags_list";

    private Spinner tagTypeSpinner;
    private EditText tagValueEditText;
    private Button cancelAddTagButton;
    private Button confirmAddTagButton;

    private ArrayList<Tag> tags;
    private ArrayList<String> availableTagTypes;
    private String selectedTagType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_tag_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        tagTypeSpinner = findViewById(R.id.tagTypeSpinner);
        tagValueEditText = findViewById(R.id.tagValueEditText);
        cancelAddTagButton = findViewById(R.id.cancelAddTagButton);
        confirmAddTagButton = findViewById(R.id.confirmAddTagButton);

        Bundle bundle = getIntent().getExtras();
        availableTagTypes = new ArrayList<String>(2);
        if (bundle != null) {
            tags = bundle.getParcelableArrayList(TAGS_LIST);
            setTagTypeSpinner();
        } else {
            Toast toast = Toast.makeText(this, "Tags could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
            cancelAddTagButton.setEnabled(false);
            confirmAddTagButton.setEnabled(false);
            cancelAddTagButton.setAlpha((float) 0.25);
            confirmAddTagButton.setAlpha((float) 0.25);
        }

        tagTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedTagType = (String) parentView.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                selectedTagType = null;
            }
        });
    }

    private void setTagTypeSpinner() {
        availableTagTypes.clear();
        availableTagTypes.add("location");
        availableTagTypes.add("person");
        for (Tag tag : tags) {
            if (tag.matchesTagType("location")) {
                availableTagTypes.remove("location");
            }
        }
        tagTypeSpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.album, availableTagTypes));
    }

    public void cancelAddTag(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void confirmAddTag(View view) {
        String tagValue = tagValueEditText.getText().toString();
        if (tagValue == null || tagValue.isEmpty() || selectedTagType == null) {
            Toast toast = Toast.makeText(this, "Both tag value and tag type are required.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
        } else {
            if (tags.contains(new Tag(selectedTagType, tagValue))) {
                Toast toast = Toast.makeText(this, "That tag already exists for this photo.", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                toastText.setTextColor(Color.WHITE);
                toast.show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString(EditTags.NEW_TAG_TYPE, selectedTagType);
                bundle.putString(EditTags.NEW_TAG_VALUE, tagValue);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

}

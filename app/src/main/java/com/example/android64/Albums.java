package com.example.android64;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class Albums extends AppCompatActivity {

    private ListView albumsListView;
    private Button deleteAlbumButton, renameAlbumButton, openAlbumButton;
    private ArrayList<Album> albums;
    private Album selectedAlbum;

    public static final String ALBUMS_LIST = "albums_list";

    private final int REQUEST_OPEN_ALBUM = 1;
    private final int REQUEST_SEARCH = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums_list);
        albumsListView = findViewById(R.id.albumsListView);
        deleteAlbumButton = findViewById(R.id.deleteAlbumButton);
        renameAlbumButton = findViewById(R.id.renameAlbumButton);
        openAlbumButton = findViewById(R.id.openAlbumButton);

        enableAlbumButtons(selectedAlbum != null);

        try {
            readAlbums();
        } catch (IOException | ClassNotFoundException e) {
            Log.i("READ ERROR", e.toString());
            albums = new ArrayList<Album>();
        }
//        albums.add(new Album("hello"));
//        albums.add(new Album("hello2"));

        ArrayAdapter<Album> adapter = new ArrayAdapter<Album>(this, R.layout.album, albums);
        albumsListView.setAdapter(adapter);
        albumsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectAlbum(position);
            }
        });
/*        try {
            writeAlbums();
        } catch (IOException e) {
            // do alert dialog
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;
            case R.id.action_search:
                search();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void selectAlbum(int index) {
        if (index >= albums.size()) {
            return;
        }
        selectedAlbum = albums.get(index);
        enableAlbumButtons(true);
    }

    public void deleteAlbum(View view) {
        deleteAlbumButton.setAlpha(1);
        Albums temp = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete the album '" + selectedAlbum.getName() + "'?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        albums.remove(selectedAlbum);
                        albumsListView.setAdapter(new ArrayAdapter<Album>(temp, R.layout.album, albums));
                        enableAlbumButtons(false);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteAlbumButton.setAlpha((float) 0.85);
                    }
                }).setTitle("Delete Album");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void openAlbumNameDialog(boolean renameMode) {
        Albums temp = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        EditText albumNameInput = new EditText(this);
/*        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(100, 0, 100, 0);
        layoutParams.setMarginStart(100);
        layoutParams.setMarginEnd(100);
        albumNameInput.setLayoutParams(layoutParams);*/
        if (renameMode) {
            albumNameInput.setText(selectedAlbum.getName());
        }
        builder.setView(albumNameInput)
                .setTitle(renameMode ? "Rename Album" : "Add Album")
                .setMessage("Enter an album name.")
                .setPositiveButton(renameMode ? "Rename" : "Add", null)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        enableAlbumButtons(selectedAlbum != null);
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button positiveButton = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name = albumNameInput.getText().toString().trim();
                        if (name == null || name.isEmpty()) {
                            Toast toast = Toast.makeText(Albums.this, "Album name is required.", Toast.LENGTH_SHORT);
                            View toastView = toast.getView();
                            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                            toastText.setTextColor(Color.WHITE);
                            toast.show();
                        } else {
                            boolean alreadyExists = false;
                            for (Album album : albums) {
                                if (!renameMode && album.getName().equalsIgnoreCase(name)) {
                                    alreadyExists = true;
                                    break;
                                } else if (album.getName().equalsIgnoreCase(name) && !album.equals(selectedAlbum)) {
                                    alreadyExists = true;
                                    break;
                                }
                            }
                            if (alreadyExists) {
                                Toast toast = Toast.makeText(Albums.this, "Another album with this name exists.", Toast.LENGTH_LONG);
                                View toastView = toast.getView();
                                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                                toastText.setTextColor(Color.WHITE);
                                toast.show();
                            } else {
                                if (renameMode) {
                                    selectedAlbum.setName(name);
                                } else {
                                    selectedAlbum = new Album(name);
                                    albums.add(selectedAlbum);
                                }
                                Collections.sort(albums);
                                albumsListView.setAdapter(new ArrayAdapter<Album>(temp, R.layout.album, albums));
                                enableAlbumButtons(false);
                                alertDialog.dismiss();
                            }
                        }
                    }
                });
            }
        });
        alertDialog.show();
    }

    private void addAlbum() {
        openAlbumNameDialog(false);
    }

    private void search() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Search.ALBUMS_LIST, albums);
        Intent intent = new Intent(this, Search.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_SEARCH);
    }

    public void renameAlbum(View view) {
        renameAlbumButton.setAlpha(1);
        openAlbumNameDialog(true);
    }

    public void openAlbum(View view) {
        openAlbumButton.setAlpha(1);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Photos.ALBUMS_LIST, albums);
        bundle.putInt(Photos.SELECTED_ALBUM_INDEX, albums.indexOf(selectedAlbum));
        Intent intent = new Intent(this, Photos.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_OPEN_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        if (requestCode == REQUEST_OPEN_ALBUM) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            albumsListView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albums));
            enableAlbumButtons(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            writeAlbums();
        } catch (IOException e) {
            // do alert dialog
        }
    }

    private void enableAlbumButtons(boolean enable) {
        if (!enable) {
            selectedAlbum = null;
        }
        deleteAlbumButton.setEnabled(enable);
        renameAlbumButton.setEnabled(enable);
        openAlbumButton.setEnabled(enable);
        deleteAlbumButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        renameAlbumButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        openAlbumButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    public void readAlbums() throws IOException, ClassNotFoundException {
        File file = new File(this.getFilesDir(), "photos.dat");
        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Unable to create storage file.");
        }
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        albums = (ArrayList<Album>) ois.readObject();
        ois.close();
    }

    public void writeAlbums() throws IOException {
        File file = new File(this.getFilesDir(), "photos.dat");
        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Unable to create storage file.");
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(albums);
        oos.close();
    }
}

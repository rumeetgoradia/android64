package com.example.android64;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchResults extends AppCompatActivity {

    public static final String SEARCH_RESULTS_LIST = "search_results_list";

    private final int REQUEST_DISPLAY_SEARCH_RESULT = 1;

    private GridView photosGrid;
    private Button displayPhotoButton;
    private TextView noPhotosTextView;

    private ArrayList<Photo> photos;
    private Photo selectedPhoto;
    private int prevSelectedPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photos = bundle.getParcelableArrayList(SEARCH_RESULTS_LIST);
        } else {
            photos = new ArrayList<Photo>();
        }

        photosGrid = findViewById(R.id.photosGrid);
        displayPhotoButton = findViewById(R.id.displayPhotoButton);
        noPhotosTextView = findViewById(R.id.noPhotosTextView);

        if (photos.size() > 0) {
            noPhotosTextView.setVisibility(View.GONE);
            photosGrid.setAdapter(new PhotoGridAdapter(this, photos));
            enableDisplayButton(selectedPhoto != null);
            photosGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position >= photos.size()) {
                        return;
                    }
                    selectedPhoto = photos.get(position);
//                    Toast.makeText(SearchResults.this, "Selected photo: " + selectedPhoto.getFilePath(), Toast.LENGTH_LONG).show();
                    enableDisplayButton(true);
                    view.setBackgroundColor(Color.parseColor("#257AFD"));
                    View prevSelectedView = photosGrid.getChildAt(prevSelectedPos);
                    if (prevSelectedPos != -1 && prevSelectedPos != position) {
                        prevSelectedView.setSelected(false);
                        prevSelectedView.setBackgroundColor(Color.TRANSPARENT);
                    }
                    prevSelectedPos = position;
                }
            });
        } else {
            photosGrid.setVisibility(View.GONE);
            enableDisplayButton(false);
        }
    }

    private void enableDisplayButton(boolean enable) {
        if (!enable) {
            selectedPhoto = null;
        }
        displayPhotoButton.setEnabled(enable);
        displayPhotoButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    public void displayPhoto(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SearchResultsDisplay.SEARCH_RESULT_TO_DISPLAY, selectedPhoto);
        bundle.putParcelableArrayList(SearchResultsDisplay.PHOTOS_LIST, photos);
        Intent intent = new Intent(this, SearchResultsDisplay.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_DISPLAY_SEARCH_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null || data.getExtras() == null) {
            return;
        }

        Bundle bundle = data.getExtras();
        photos = bundle.getParcelableArrayList(SEARCH_RESULTS_LIST);
        photosGrid.setAdapter(new PhotoGridAdapter(this, photos));
        enableDisplayButton(false);
    }
}

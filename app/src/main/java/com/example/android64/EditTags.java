package com.example.android64;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class EditTags extends AppCompatActivity {

    public final static String ALBUMS_LIST = "albums_list";
    public final static String SELECTED_ALBUM_INDEX = "selected_album_index";
    public static final String SELECTED_PHOTO_INDEX = "selected_photo_index";
//    public final static String TAGS_LIST = "tags_list";
    public final static String NEW_TAG_TYPE = "new_tag_type";
    public final static String NEW_TAG_VALUE = "new_tag_value";

    private final int REQUEST_ADD_TAG = 1;

    private ListView tagsListView;
    private Button deleteTagButton;

    private ArrayList<Album> albums;
    private Album album;
    private Photo photo;
    private Tag selectedTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_tags_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(DisplayPhoto.ALBUMS_LIST, albums);
                bundle.putInt(DisplayPhoto.SELECTED_ALBUM_INDEX, albums.indexOf(album));
                bundle.putInt(DisplayPhoto.SELECTED_PHOTO_INDEX, album.getPhotos().indexOf(photo));
//                bundle.putParcelableArrayList(DisplayPhoto.NEW_TAGS_LIST, tags);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        tagsListView = findViewById(R.id.tagsBigListView);
        deleteTagButton = findViewById(R.id.deleteTagButton);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            album = albums.get(bundle.getInt(SELECTED_ALBUM_INDEX));
            photo = album.getPhotos().get(bundle.getInt(SELECTED_PHOTO_INDEX));
            TagsAdapter tagsAdapter = new TagsAdapter(this, photo.getTags());
            tagsListView.setAdapter(tagsAdapter);
        } else {
            Toast toast = Toast.makeText(this, "Tags for the selected photo could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
            enableDeleteTagButton(false);
        }

        enableDeleteTagButton(selectedTag != null);

        tagsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectTag(position);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                if (photo.getTags() != null) {
                  addTag();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void enableDeleteTagButton(boolean enable) {
        deleteTagButton.setEnabled(enable);
        deleteTagButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    private void selectTag(int index) {
        if (index >= photo.getTags().size()) {
            return;
        }
        selectedTag = photo.getTags().get(index);
        enableDeleteTagButton(true);
    }

    public void addTag() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(AddTag.TAGS_LIST, photo.getTags());
        Intent intent = new Intent(this, AddTag.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_ADD_TAG);
    }

    public void deleteTag(View view) {
        deleteTagButton.setAlpha(1);
        EditTags temp = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete the tag '" + selectedTag.getTagType() + ": " + selectedTag.getTagValue() + "'?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        photo.getTags().remove(selectedTag);
                        Collections.sort(photo.getTags());
                        tagsListView.setAdapter(new TagsAdapter(temp, photo.getTags()));
                        enableDeleteTagButton(false);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteTagButton.setAlpha((float) 0.85);
                    }
                }).setTitle("Delete Tag");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null || data.getExtras() == null) {
            return;
        }
        Bundle bundle = data.getExtras();
        if (requestCode == REQUEST_ADD_TAG) {
            String tagType = bundle.getString(NEW_TAG_TYPE);
            String tagValue = bundle.getString(NEW_TAG_VALUE);
            photo.getTags().add(new Tag(tagType, tagValue));
            Collections.sort(photo.getTags());
            TagsAdapter tagsAdapter = new TagsAdapter(this, photo.getTags());
            tagsListView.setAdapter(tagsAdapter);
            enableDeleteTagButton(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            writeAlbums();
        } catch (IOException e) {
            // do alert dialog
        }
    }

    public void writeAlbums() throws IOException {
        File file = new File(this.getFilesDir(), "photos.dat");
        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Unable to create storage file.");
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(albums);
        oos.close();
    }
}

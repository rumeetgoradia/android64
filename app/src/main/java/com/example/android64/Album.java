package com.example.android64;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Album implements Serializable, Parcelable, Comparable<Album> {
    private String name;
    private ArrayList<Photo> photos;


    public Album(String name, ArrayList<Photo> photos) {
        this.name = name;
        this.photos = photos;
    }

    public Album(String name) {
        this(name, new ArrayList<Photo>());
    }

    protected Album(Parcel in) {
        this(in.readString(), in.readArrayList(Photo.class.getClassLoader()));
    }


    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeList(this.photos);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean matchesName(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    public ArrayList<Photo> getPhotos() {
        return this.photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
        Collections.sort(this.photos);
    }

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
        Collections.sort(this.photos);
    }

    public void removePhoto(Photo photo) {
        this.photos.remove(photo);
        Collections.sort(this.photos);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Album)) {
            return false;
        }
        Album a = (Album) o;
        return a.getName().equals(this.name);
    }

    public int compareTo(Album a) {
        if (a == null) {
            return 1;
        }
        return this.name.compareTo(a.getName());
    }

    public String toString() {
        return this.name;
    }
}

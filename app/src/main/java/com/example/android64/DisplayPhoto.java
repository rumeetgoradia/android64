package com.example.android64;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.transition.Slide;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class DisplayPhoto extends AppCompatActivity {

    public static final String ALBUMS_LIST = "albums_list";
    public static final String SELECTED_ALBUM_INDEX = "selected_album_index";
    public static final String SELECTED_PHOTO_INDEX = "selected_photo_index";
    public static final String NEW_TAGS_LIST = "new_tags_list";

    private final int REQUEST_SLIDESHOW = 1;
    private final int REQUEST_EDIT_TAGS = 2;

    private ImageView photoImageView;
    private ListView tagsListView;
    private Button editTagsButton;
    private Button slideshowButton;

    private ArrayList<Album> albums;
    private Album album;
    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_photo_container);

        photoImageView = findViewById(R.id.photoDisplayImageView);
        tagsListView = findViewById(R.id.tagsListView);
        editTagsButton = findViewById(R.id.editTagsButton);
        slideshowButton = findViewById(R.id.slideshowButton);

        setTitle("Display Photo");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Photos.UPDATED_PHOTO_TAGS, photo.getTags());
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            album = albums.get(bundle.getInt(SELECTED_ALBUM_INDEX));
            photo = album.getPhotos().get(bundle.getInt(SELECTED_PHOTO_INDEX));
            String filePath = photo.getFilePath();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            photoImageView.setImageBitmap(bitmap);
            TagsAdapter tagsAdapter = new TagsAdapter(this, photo.getTags());
            tagsListView.setAdapter(tagsAdapter);
            enableDisplayButtons(true);
        } else {
            Toast toast = Toast.makeText(this, "Photo could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
            enableDisplayButtons(false);
        }


    }

    private void enableDisplayButtons(boolean enable) {
        editTagsButton.setEnabled(enable);
        slideshowButton.setEnabled(enable);
        editTagsButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        slideshowButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    public void startSlideshow(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Slideshow.ALBUMS_LIST, albums);
        bundle.putInt(Slideshow.SELECTED_ALBUM_INDEX, albums.indexOf(album));
        bundle.putInt(Slideshow.CURRENT_PHOTO_INDEX, album.getPhotos().indexOf(photo));
//        bundle.putParcelable(Slideshow.SELECTED_ALBUM, album);
//        bundle.putInt(Slideshow.CURRENT_INDEX, album.getPhotos().indexOf(photo));
        Intent intent = new Intent(this, Slideshow.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_SLIDESHOW);
    }

    public void editTags(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(EditTags.ALBUMS_LIST, albums);
        bundle.putInt(EditTags.SELECTED_ALBUM_INDEX, albums.indexOf(album));
        bundle.putInt(EditTags.SELECTED_PHOTO_INDEX, album.getPhotos().indexOf(photo));
        Intent intent = new Intent(this, EditTags.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_EDIT_TAGS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (data == null || data.getExtras() == null) {
            return;
        }

        Bundle bundle = data.getExtras();
        if (requestCode == REQUEST_EDIT_TAGS) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            album = albums.get(bundle.getInt(SELECTED_ALBUM_INDEX));
            photo = album.getPhotos().get(bundle.getInt(SELECTED_PHOTO_INDEX));
            String filePath = photo.getFilePath();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            photoImageView.setImageBitmap(bitmap);
            TagsAdapter tagsAdapter = new TagsAdapter(this, photo.getTags());
            tagsListView.setAdapter(tagsAdapter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            writeAlbums();
        } catch (IOException e) {
            // do alert dialog
        }
    }

    public void writeAlbums() throws IOException {
        File file = new File(this.getFilesDir(), "photos.dat");
        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Unable to create storage file.");
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(albums);
        oos.close();
    }
}

package com.example.android64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AtomicFile;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Photos extends AppCompatActivity {

    private GridView photosGrid;
    private Button removePhotoButton;
    private Button movePhotoButton;
    private Button displayPhotoButton;
    private TextView noPhotosTextView;

    private ArrayList<Album> albums;
    private Album selectedAlbum;
    private int selectedAlbumIndex;

    private Photo selectedPhoto;
    private int prevSelectedPos = -1;

    private final int REQUEST_PICK_IMAGE = 1;
    private final int REQUEST_MOVE_PHOTO = 2;
    private final int REQUEST_DISPLAY_PHOTO = 3;

    public final static String ALBUMS_LIST = "albums_list";
    public final static String SELECTED_ALBUM_INDEX = "selected_album_index";
    public final static String UPDATED_PHOTO_TAGS = "updated_photo_tags";
    public final static String COPY_OR_MOVE = "copy_or_move";
    public final static String MOVE_ALBUM = "move_album";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_list_container);

        photosGrid = findViewById(R.id.photosGrid);
        removePhotoButton = findViewById(R.id.removePhotoButton);
        movePhotoButton = findViewById(R.id.movePhotoButton);
        displayPhotoButton = findViewById(R.id.displayPhotoButton);
        noPhotosTextView = findViewById(R.id.noPhotosTextView);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Albums.ALBUMS_LIST, albums);
                Intent intent = new Intent(Photos.this, Albums.class);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            selectedAlbumIndex = bundle.getInt(SELECTED_ALBUM_INDEX);
            selectedAlbum = albums.get(selectedAlbumIndex);
            setTitle(selectedAlbum.getName());
        } else {
//            photos = new ArrayList<Photo>();
            setTitle("Photos Display");
            Toast toast = Toast.makeText(this, "Photos could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
        }

        enablePhotoButtons(selectedPhoto != null);

        PhotoGridAdapter photoGridAdapter = new PhotoGridAdapter(this, selectedAlbum.getPhotos());
        photosGrid.setAdapter(photoGridAdapter);
        photosGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position >= selectedAlbum.getPhotos().size()) {
                    return;
                }
                selectedPhoto = selectedAlbum.getPhotos().get(position);
                enablePhotoButtons(true);
                view.setBackgroundColor(Color.parseColor("#257AFD"));
                View prevSelectedView = photosGrid.getChildAt(prevSelectedPos);
                if (prevSelectedPos != -1 && prevSelectedPos != position) {
                    prevSelectedView.setSelected(false);
                    prevSelectedView.setBackgroundColor(Color.TRANSPARENT);
                }
                prevSelectedPos = position;
//                Toast.makeText(Photos.this, selectedPhoto.getFilePath(), Toast.LENGTH_LONG).show();
//                Toast.makeText(Photos.this, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath(), Toast.LENGTH_LONG).show();
            }
        });

        if (selectedAlbum.getPhotos().size() > 0) {
            noPhotosTextView.setVisibility(View.GONE);
            enablePhotoButtons(false);
        } else {
            photosGrid.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addPhoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            writeAlbums();
        } catch (IOException e) {
            // do alert dialog
        }
    }

    private void enablePhotoButtons(boolean enable) {
        if (!enable) {
            selectedPhoto = null;
            prevSelectedPos = -1;
        }
        removePhotoButton.setEnabled(enable);
        movePhotoButton.setEnabled(enable);
        displayPhotoButton.setEnabled(enable);
        removePhotoButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        movePhotoButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        displayPhotoButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PICK_IMAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
                } else {
                    Toast toast = Toast.makeText(this, "Gallery permissions were not granted.", Toast.LENGTH_LONG);
                    View toastView = toast.getView();
                    toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                    TextView toastText = toastView.findViewById(android.R.id.message);
                    toastText.setTextColor(Color.WHITE);
                    toast.show();
                }
                break;
        }
    }

    private void addPhoto() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PICK_IMAGE);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
        }

    }

    public void removePhoto(View view) {
        removePhotoButton.setAlpha(1);
        Photos temp = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to remove this photo from the album '" + selectedAlbum.getName() + "'?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectedAlbum.removePhoto(selectedPhoto);
                        photosGrid.setAdapter(new PhotoGridAdapter(temp, selectedAlbum.getPhotos()));
                        enablePhotoButtons(false);
                        if (selectedAlbum.getPhotos().size() > 0) {
                            noPhotosTextView.setVisibility(View.GONE);
                            photosGrid.setVisibility(View.VISIBLE);
                        } else {
                            photosGrid.setVisibility(View.GONE);
                            noPhotosTextView.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        removePhotoButton.setAlpha((float) 0.85);
                    }
                }).setTitle("Remove Photo");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void movePhoto(View view) {
        movePhotoButton.setAlpha(1);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(MovePhoto.ALBUMS_LIST, albums);
        bundle.putInt(MovePhoto.CURRENT_ALBUM_INDEX, selectedAlbumIndex);
        Intent intent = new Intent(this, MovePhoto.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_MOVE_PHOTO);
    }

    public void displayPhoto(View view) {
        displayPhotoButton.setAlpha(1);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(DisplayPhoto.ALBUMS_LIST, albums);
        bundle.putInt(DisplayPhoto.SELECTED_ALBUM_INDEX, selectedAlbumIndex);
        bundle.putInt(DisplayPhoto.SELECTED_PHOTO_INDEX, selectedAlbum.getPhotos().indexOf(selectedPhoto));
        Intent intent = new Intent(this, DisplayPhoto.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_DISPLAY_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) {
            return;
        }

        if (requestCode == REQUEST_PICK_IMAGE) {
            Uri uri = data.getData();
            Photo photoToAdd = new Photo(getFilePathFromUri(uri));
            if (!selectedAlbum.getPhotos().contains(photoToAdd)) {
                selectedAlbum.addPhoto(photoToAdd);
                PhotoGridAdapter photoGridAdapter = new PhotoGridAdapter(this, selectedAlbum.getPhotos());
                photosGrid.setAdapter(photoGridAdapter);
                noPhotosTextView.setVisibility(View.GONE);
                photosGrid.setVisibility(View.VISIBLE);
                enablePhotoButtons(false);
            } else {
                Toast toast = Toast.makeText(this, "That photo already exists in this album.", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                toastText.setTextColor(Color.WHITE);
                toast.show();
            }
        } else if (requestCode == REQUEST_MOVE_PHOTO) {
            Bundle bundle = data.getExtras();
            String albumForMoveName = bundle.getString(MOVE_ALBUM);
            String copyOrMove = bundle.getString(COPY_OR_MOVE);
            Album albumForMove = null;
            for (Album album : albums) {
                if (album.matchesName(albumForMoveName)) {
                    albumForMove = album;
                }
            }
            if (albumForMove != null) {
                if (albumForMove.getPhotos().contains(selectedPhoto)) {
                    Toast toast = Toast.makeText(this, "This photo already exists in the album '" + albumForMoveName + "'.", Toast.LENGTH_LONG);
                    View toastView = toast.getView();
                    toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                    TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                    toastText.setTextColor(Color.WHITE);
                    toast.show();
                    enablePhotoButtons(true);
                } else {
                    if (copyOrMove.equals("move")) {
                        selectedAlbum.removePhoto(selectedPhoto);
                    }
                    albumForMove.addPhoto(selectedPhoto);
                    photosGrid.setAdapter(new PhotoGridAdapter(this, selectedAlbum.getPhotos()));
                    if (selectedAlbum.getPhotos().size() > 0) {
                        noPhotosTextView.setVisibility(View.GONE);
                        photosGrid.setVisibility(View.VISIBLE);
                    } else {
                        photosGrid.setVisibility(View.GONE);
                        noPhotosTextView.setVisibility(View.VISIBLE);
                    }
                    enablePhotoButtons(false);
                }
            } else {
                Toast toast = Toast.makeText(this, "Could not locate the album '" + albumForMoveName + "'.", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                toastText.setTextColor(Color.WHITE);
                toast.show();
                enablePhotoButtons(true);
            }
        } else if (requestCode == REQUEST_DISPLAY_PHOTO) {
            Bundle bundle = data.getExtras();
            ArrayList<Tag> updatedTags = bundle.getParcelableArrayList(UPDATED_PHOTO_TAGS);
            selectedPhoto.setTags(updatedTags);
            photosGrid.setAdapter(new PhotoGridAdapter(this, selectedAlbum.getPhotos()));
            enablePhotoButtons(false);
        }
    }

    private String getFilePathFromUri(Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(
                uri, proj, null, null, null
        );
        int colIndex = cursor.getColumnIndexOrThrow(proj[0]);
        cursor.moveToFirst();
        String result = cursor.getString(colIndex);
        cursor.close();

        return result;
    }

    public void writeAlbums() throws IOException {
        File file = new File(this.getFilesDir(), "photos.dat");
        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Unable to create storage file.");
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(albums);
        oos.close();
    }
}

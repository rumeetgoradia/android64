package com.example.android64;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Tag implements Serializable, Parcelable, Comparable<Tag> {
    private String tagType;
    private String tagValue;

    public Tag(String tagType, String tagValue) {
        this.tagType = tagType;
        this.tagValue = tagValue;
    }

    public String getTagType() {
        return tagType;
    }

    public String getTagValue() {
        return tagValue;
    }

    public boolean matchesTagType(String tagType) {
        return this.tagType.equals(tagType);
    }

    public boolean matchesTagValue(String tagValue) {
        return this.tagValue.equals(tagValue) || this.tagValue.indexOf(tagValue) == 0;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof Tag)) {
            return false;
        }
        Tag t = (Tag) o;
        return t.getTagType().equals(this.tagType) && t.getTagValue().equals(this.tagValue);
    }

    public int compareTo(Tag t) {
        if (t.getTagType().equalsIgnoreCase(this.tagType)) {
            return this.tagValue.compareTo(t.getTagValue());
        }
        return this.tagType.compareTo(t.getTagType());
    }

    protected Tag(Parcel in) {
        this(in.readString(), in.readString());
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.tagType);
        parcel.writeString(this.tagValue);
    }

}

package com.example.android64;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class Search extends AppCompatActivity {

    public static final String ALBUMS_LIST = "albums_list";

    private final int REQUEST_SEARCH_RESULTS = 1;

    private Spinner tagTypeSpinner;
    private EditText tagValueEditText;
    private TextView secondTagTypeTextView;
    private TextView secondTagValueTextView;
    private Spinner secondTagTypeSpinner;
    private EditText secondTagValueEditText;
    private CheckBox andCheckBox;
    private CheckBox orCheckBox;

    private ArrayList<Album> albums;
    private String selectedTagType;
    private String secondSelectedTagType;
    private ArrayAdapter<String> availableTagTypes;
    private ArrayAdapter<String> secondAvailableTagTypes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tagTypeSpinner = findViewById(R.id.tagTypeSpinner);
        tagValueEditText = findViewById(R.id.tagValueEditText);
        secondTagTypeTextView = findViewById(R.id.searchTextView3);
        secondTagValueTextView = findViewById(R.id.searchTextView4);
        secondTagTypeSpinner = findViewById(R.id.secondTagTypeSpinner);
        secondTagValueEditText = findViewById(R.id.secondTagValueEditText);
        andCheckBox = findViewById(R.id.andCheckBox);
        orCheckBox = findViewById(R.id.orCheckBox);

        hideSecondaries(true);

        Bundle bundle = getIntent().getExtras();
//        Log.e("REPORTING IF BUNDLE IS NULL", String.valueOf(bundle == null));
        if (bundle != null) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
        } else {
            albums = new ArrayList<>();
        }

        ArrayList<String> tagTypes = new ArrayList<String>(2);
        tagTypes.add("location");
        tagTypes.add("person");
        availableTagTypes = new ArrayAdapter<>(this, R.layout.album, tagTypes);
        secondAvailableTagTypes = new ArrayAdapter<>(this, R.layout.album);
        secondAvailableTagTypes.add("person");
        tagTypeSpinner.setAdapter(availableTagTypes);
        secondTagTypeSpinner.setAdapter(secondAvailableTagTypes);
        tagTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedTagType = (String) parentView.getItemAtPosition(position);
                if (selectedTagType.equals("location")) {
                    secondAvailableTagTypes.remove("location");
                } else {
                    boolean containsLocation = false;
                    for (int i = 0; i < secondAvailableTagTypes.getCount(); ++i) {
                        if (((String) secondAvailableTagTypes.getItem(i)).equals("location")) {
                            containsLocation = true;
                        }
                    }
                    if (!containsLocation) {
                        secondAvailableTagTypes.add("location");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                selectedTagType = null;
                boolean containsLocation = false;
                for (int i = 0; i < secondAvailableTagTypes.getCount(); ++i) {
                    if (((String) secondAvailableTagTypes.getItem(i)).equals("location")) {
                        containsLocation = true;
                    }
                }
                if (!containsLocation) {
                    secondAvailableTagTypes.add("location");
                }
            }
        });
        secondTagTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                secondSelectedTagType = (String) parentView.getItemAtPosition(position);
                if (secondSelectedTagType.equals("location")) {
                    availableTagTypes.remove("location");
                } else {
                    boolean containsLocation = false;
                    for (int i = 0; i < availableTagTypes.getCount(); ++i) {
                        if (((String) availableTagTypes.getItem(i)).equals("location")) {
                            containsLocation = true;
                        }
                    }
                    if (!containsLocation) {
                        availableTagTypes.add("location");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                secondSelectedTagType = null;
                boolean containsLocation = false;
                for (int i = 0; i < availableTagTypes.getCount(); ++i) {
                    if (((String) availableTagTypes.getItem(i)).equals("location")) {
                        containsLocation = true;
                    }
                }
                if (!containsLocation) {
                    availableTagTypes.add("location");
                }
            }
        });
        andCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    hideSecondaries(false);
                    orCheckBox.setEnabled(false);
                    orCheckBox.setAlpha((float) 0.25);
                } else {
                    hideSecondaries(true);
                    orCheckBox.setEnabled(true);
                    orCheckBox.setAlpha((float) 1.0);
                    secondAvailableTagTypes.remove("location");
                    boolean containsLocation = false;
                    for (int i = 0; i < availableTagTypes.getCount(); ++i) {
                        if (((String) availableTagTypes.getItem(i)).equals("location")) {
                            containsLocation = true;
                        }
                    }
                    if (!containsLocation) {
                        availableTagTypes.add("location");
                    }
                }
            }
        });
        orCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    hideSecondaries(false);
                    andCheckBox.setEnabled(false);
                    andCheckBox.setAlpha((float) 0.25);
                } else {
                    hideSecondaries(true);
                    andCheckBox.setEnabled(true);
                    andCheckBox.setAlpha((float) 1.0);
                    secondAvailableTagTypes.remove("location");
                    boolean containsLocation = false;
                    for (int i = 0; i < availableTagTypes.getCount(); ++i) {
                        if (((String) availableTagTypes.getItem(i)).equals("location")) {
                            containsLocation = true;
                        }
                    }
                    if (!containsLocation) {
                        availableTagTypes.add("location");
                    }
                }
            }
        });
    }

    private void hideSecondaries(boolean hide) {
        if (hide) {
            secondTagTypeTextView.setVisibility(View.GONE);
            secondTagValueTextView.setVisibility(View.GONE);
            secondTagTypeSpinner.setVisibility(View.GONE);
            secondTagValueEditText.setVisibility(View.GONE);
        } else {
            secondTagTypeTextView.setVisibility(View.VISIBLE);
            secondTagValueTextView.setVisibility(View.VISIBLE);
            secondTagTypeSpinner.setVisibility(View.VISIBLE);
            secondTagValueEditText.setVisibility(View.VISIBLE);
        }
    }

    public void cancelSearch(View view) {
        findViewById(R.id.cancelSearchButton).setAlpha(1);
        setResult(RESULT_CANCELED);
        finish();
    }

    public void confirmSearch(View view) {
        String tagValue = tagValueEditText.getText().toString().trim();
        findViewById(R.id.confirmSearchButton).setAlpha(1);
        ArrayList<Photo> searchResults = new ArrayList<Photo>();
        if (!andCheckBox.isChecked() && !orCheckBox.isChecked()) {
            if (tagValue == null || tagValue.isEmpty() || selectedTagType == null) {
                Toast toast = Toast.makeText(this, "Both tag value and tag type are required.", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                toastText.setTextColor(Color.WHITE);
                toast.show();
                findViewById(R.id.confirmSearchButton).setAlpha((float) 0.85);
                return;
            }
            for (Album album : albums) {
                for (Photo photo : album.getPhotos()) {
                    for (Tag tag : photo.getTags()) {
                        if (tag.matchesTagType(selectedTagType) && tag.matchesTagValue(tagValue) && !searchResults.contains(photo)) {
                            searchResults.add(photo);
                            break;
                        }
                    }
                }
            }
        } else {
            String secondTagValue = secondTagValueEditText.getText().toString().trim();
            if (tagValue == null || tagValue.isEmpty() || selectedTagType == null || secondTagValue == null || secondTagValue.isEmpty() || secondSelectedTagType == null) {
                Toast toast = Toast.makeText(this, "Both tag value and tag type are required for both tags.", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
                TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
                toastText.setTextColor(Color.WHITE);
                toast.show();
                findViewById(R.id.confirmSearchButton).setAlpha((float) 0.85);
                return;
            }

            if (andCheckBox.isChecked()) {
                ArrayList<Photo> matchesFirstTag = new ArrayList<Photo>();
                for (Album album : albums) {
                    for (Photo photo : album.getPhotos()) {
                        for (Tag tag : photo.getTags()) {
                            if (tag.matchesTagType(selectedTagType) && tag.matchesTagValue(tagValue) && !matchesFirstTag.contains(photo)) {
                                matchesFirstTag.add(photo);
                                break;
                            }
                        }
                    }
                }
                for (Photo photo : matchesFirstTag) {
                    boolean matchesSecondTag = false;
                    for (Tag tag : photo.getTags()) {
                        if (tag.matchesTagType(secondSelectedTagType) && tag.matchesTagValue(secondTagValue)) {
                            matchesSecondTag = true;
                            break;
                        }
                    }
                    if (matchesSecondTag) {
                        searchResults.add(photo);
                    }
                }
            } else {
                for (Album album : albums) {
                    for (Photo photo : album.getPhotos()) {
                        for (Tag tag : photo.getTags()) {
                            boolean matchesFirstTag = tag.matchesTagType(selectedTagType) && tag.matchesTagValue(tagValue);
                            boolean matchesSecondTag = tag.matchesTagType(secondSelectedTagType) && tag.matchesTagValue(secondTagValue);
                            if ((matchesFirstTag || matchesSecondTag) && !searchResults.contains(photo)) {
                                searchResults.add(photo);
                                break;
                            }
                        }
                    }
                }
            }
        }
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SearchResults.SEARCH_RESULTS_LIST, searchResults);
        Intent intent = new Intent(this, SearchResults.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_SEARCH_RESULTS);
    }

}

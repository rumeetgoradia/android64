package com.example.android64;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchResultsDisplay extends AppCompatActivity {

    public static final String SEARCH_RESULT_TO_DISPLAY = "search_result_to_display";
    public static final String PHOTOS_LIST = "photos_list";

    private ImageView searchDisplayImageView;
    private ListView searchTagsListView;

    private ArrayList<Photo> photos;
    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_display_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchResults.SEARCH_RESULTS_LIST, photos);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        searchDisplayImageView = findViewById(R.id.searchDisplayImageView);
        searchTagsListView = findViewById(R.id.searchTagsListView);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photo = bundle.getParcelable(SEARCH_RESULT_TO_DISPLAY);
            photos = bundle.getParcelableArrayList(PHOTOS_LIST);
            String filePath = photo.getFilePath();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            searchDisplayImageView.setImageBitmap(bitmap);
            TagsAdapter tagsAdapter = new TagsAdapter(this, photo.getTags());
            searchTagsListView.setAdapter(tagsAdapter);
        } else {
            Toast toast = Toast.makeText(this, "Photo could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
        }

    }

}


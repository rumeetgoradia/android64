package com.example.android64;

import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable, Parcelable, Comparable<Photo> {
//    private String uriString;
    private String filePath;
    private ArrayList<Tag> tags;

    public Photo(String filePath, ArrayList<Tag> tags) {
//        this.uriString = uriString;
        this.filePath = filePath;
        this.tags = tags;
    }

    public Photo(String filePath) {
        this(filePath, new ArrayList<Tag>());
    }

//    public String getUriString() {
//        return this.uriString;
//    }

//    public Uri getUri() {
//        return Uri.parse(this.uriString);
//    }

    public String getFilePath() {
        return this.filePath;
    }

    public ArrayList<Tag> getTags() {
        return this.tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Photo)) {
            return false;
        }
        Photo p = (Photo) o;
        return p.filePath.equals(this.filePath);
    }

    public int compareTo(Photo p) {
        if (p == null) {
            return 1;
        }
        return this.filePath.compareTo(p.getFilePath());
    }

    protected Photo(Parcel in) {
        this(in.readString(), (ArrayList<Tag>) in.readArrayList(Tag.class.getClassLoader()));
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(this.uriString);
        dest.writeString(this.filePath);
        dest.writeList(this.tags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}

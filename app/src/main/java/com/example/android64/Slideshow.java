package com.example.android64;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Slideshow extends AppCompatActivity {

    public static final String ALBUMS_LIST = "albums_list";
    public static final String SELECTED_ALBUM_INDEX = "selected_album_index";
    public static final String CURRENT_PHOTO_INDEX = "current_index";

    private Button prevButton;
    private Button nextButton;
    private ImageView slideshowImageView;
    private ListView slideshowTagsListView;

    private ArrayList<Album> albums;
    private Album album;
    private int originalIndex;
    private int currentIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow_container);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(DisplayPhoto.ALBUMS_LIST, albums);
                bundle.putInt(DisplayPhoto.SELECTED_ALBUM_INDEX, albums.indexOf(album));
                bundle.putInt(DisplayPhoto.SELECTED_PHOTO_INDEX, originalIndex);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        prevButton = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);
        slideshowImageView = findViewById(R.id.slideshowImageView);
        slideshowTagsListView = findViewById(R.id.slideshowTagsListView);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albums = bundle.getParcelableArrayList(ALBUMS_LIST);
            album = albums.get(bundle.getInt(SELECTED_ALBUM_INDEX));
            setTitle(album.getName() + " Slideshow");
            originalIndex = bundle.getInt(CURRENT_PHOTO_INDEX);
            currentIndex = originalIndex;
            setDisplayedImage();
            enableSlideshowButtons(true);
        } else {
            Toast toast = Toast.makeText(this, "Album could not be loaded.", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.getBackground().setColorFilter(Color.parseColor("#A63232"), PorterDuff.Mode.SRC_IN);
            TextView toastText = (TextView) toastView.findViewById(android.R.id.message);
            toastText.setTextColor(Color.WHITE);
            toast.show();
            enableSlideshowButtons(false);
        }

    }

    private void enableSlideshowButtons(boolean enable) {
        prevButton.setEnabled(enable);
        nextButton.setEnabled(enable);
        prevButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
        nextButton.setAlpha(enable ? (float) 0.85 : (float) 0.25);
    }

    private void setDisplayedImage() {
        String filePath = album.getPhotos().get(currentIndex).getFilePath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        slideshowImageView.setImageBitmap(bitmap);
        slideshowTagsListView.setAdapter(new TagsAdapter(this, album.getPhotos().get(currentIndex).getTags()));
    }

    public void nextPhoto(View view) {
        currentIndex = currentIndex == album.getPhotos().size() - 1 ? 0 : currentIndex + 1;
        setDisplayedImage();
    }

    public void prevPhoto(View view) {
        currentIndex = currentIndex == 0 ? album.getPhotos().size() - 1 : currentIndex - 1;
        setDisplayedImage();
    }
}
